package br.com.esilvasoft.geradorestrutural.json;

import java.util.ArrayList;
import java.util.List;

import br.com.esilvasoft.geradorestrutural.comando.ComandoCriarEntidades;
import br.com.esilvasoft.geradorestrutural.modelo.Projeto;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public interface ComandoCriarEntidadesJsonConversor {

	ComandoCriarEntidades paraEntidade(final Projeto projeto, final JsonObject jsonObject);

	default ComandoCriarEntidades paraEntidade(final Projeto projeto, final String json) {
		final JsonObject jsonObject = new JsonObject(json);
		return this.paraEntidade(projeto, jsonObject);
	};

	default List<ComandoCriarEntidades> paraLista(final Projeto projeto, final JsonArray jsonArray) {
		final List<ComandoCriarEntidades> lista = new ArrayList<ComandoCriarEntidades>();
		for (final Object object : jsonArray) {
			lista.add(this.paraEntidade(projeto, (JsonObject) object));
		}
		return lista;
	};

	default List<ComandoCriarEntidades> paraLista(final Projeto projeto, final String json) {
		final JsonArray jsonArray = new JsonArray(json);
		return this.paraLista(projeto, jsonArray);
	};

}