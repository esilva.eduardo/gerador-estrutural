package br.com.esilvasoft.geradorestrutural.comando;

import java.util.List;

import br.com.esilvasoft.geradorestrutural.modelo.ChaveValor;
import br.com.esilvasoft.geradorestrutural.modelo.Projeto;

public class ComandoCriarEntidade {

	private final String nome;
	private final String nomeLeiaute;
	private final Projeto projeto;
	private final List<ChaveValor> atributos;

	public ComandoCriarEntidade(final Projeto projeto, final String nome, final String nomeLeiaute,
			final List<ChaveValor> atributos) {
		super();
		this.nome = nome;
		this.nomeLeiaute = nomeLeiaute;
		this.atributos = atributos;
		this.projeto = projeto;
	}

	public List<ChaveValor> getAtributos() {
		return this.atributos;
	}

	public String getNome() {
		return this.nome;
	}

	public String getNomeLeiaute() {
		return this.nomeLeiaute;
	}

	public Projeto getProjeto() {
		return this.projeto;
	}

}
