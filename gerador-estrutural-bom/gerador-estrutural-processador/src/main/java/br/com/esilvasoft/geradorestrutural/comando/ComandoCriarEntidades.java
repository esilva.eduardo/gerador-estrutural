package br.com.esilvasoft.geradorestrutural.comando;

import java.util.List;

import br.com.esilvasoft.geradorestrutural.modelo.ChaveValor;
import br.com.esilvasoft.geradorestrutural.modelo.Projeto;

public class ComandoCriarEntidades {

	private final Projeto projeto;
	private final String nomeLeiaute;
	private final List<String> nomes;
	private final List<ChaveValor> atributos;

	public ComandoCriarEntidades(final Projeto projeto, final String nomeLeiaute, final List<String> nomes,
			final List<ChaveValor> atributos) {
		super();
		this.projeto = projeto;
		this.nomeLeiaute = nomeLeiaute;
		this.nomes = nomes;
		this.atributos = atributos;
	}

	public List<ChaveValor> getAtributos() {
		return this.atributos;
	}

	public String getNomeLeiaute() {
		return this.nomeLeiaute;
	}

	public List<String> getNomes() {
		return this.nomes;
	}

	public Projeto getProjeto() {
		return this.projeto;
	}

}
