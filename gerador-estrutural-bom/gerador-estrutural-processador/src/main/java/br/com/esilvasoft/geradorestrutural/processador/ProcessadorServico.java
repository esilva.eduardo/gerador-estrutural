package br.com.esilvasoft.geradorestrutural.processador;

import javax.enterprise.context.ApplicationScoped;

import br.com.esilvasoft.geradorestrutural.comando.ComandoCriarEntidade;
import br.com.esilvasoft.geradorestrutural.comando.ComandoCriarEntidades;
import br.com.esilvasoft.geradorestrutural.modelo.Contexto;
import br.com.esilvasoft.geradorestrutural.retorno.RetornoExecucao;

@ApplicationScoped
public interface ProcessadorServico {

	public RetornoExecucao executar(final Contexto contexto, final ComandoCriarEntidade comando);

	public RetornoExecucao executar(final Contexto contexto, final ComandoCriarEntidades comando);
}
