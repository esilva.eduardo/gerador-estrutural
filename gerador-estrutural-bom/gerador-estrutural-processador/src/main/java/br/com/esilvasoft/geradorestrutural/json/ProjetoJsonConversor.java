package br.com.esilvasoft.geradorestrutural.json;

import br.com.esilvasoft.geradorestrutural.modelo.Projeto;

public interface ProjetoJsonConversor extends JsonConversorInterface<Projeto> {
}
