package br.com.esilvasoft.geradorestrutural.json;

import br.com.esilvasoft.geradorestrutural.modelo.ChaveValor;

public interface ChaveValorJsonConversor extends JsonConversorInterface<ChaveValor> {
}
