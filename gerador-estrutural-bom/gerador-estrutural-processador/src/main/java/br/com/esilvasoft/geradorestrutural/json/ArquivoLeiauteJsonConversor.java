package br.com.esilvasoft.geradorestrutural.json;

import br.com.esilvasoft.geradorestrutural.modelo.ArquivoLeiaute;

public interface ArquivoLeiauteJsonConversor extends JsonConversorInterface<ArquivoLeiaute> {
}
