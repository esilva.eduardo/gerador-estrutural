package br.com.esilvasoft.geradorestrutural.json;

import br.com.esilvasoft.geradorestrutural.modelo.Leiaute;

public interface LeiauteJsonConversor extends JsonConversorInterface<Leiaute> {
}
