package br.com.esilvasoft.geradorestrutural.json;

import java.util.ArrayList;
import java.util.List;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public interface JsonConversorInterface<E> {

	E paraEntidade(final JsonObject jsonObject);

	default E paraEntidade(final String json) {
		final JsonObject jsonObject = new JsonObject(json);
		return this.paraEntidade(jsonObject);
	}

	default List<E> paraLista(final JsonArray jsonArray) {
		final List<E> lista = new ArrayList<>();
		jsonArray.forEach(it -> lista.add(this.paraEntidade((JsonObject) it)));
		return lista;
	}

	default List<E> paraLista(final String json) {
		final JsonArray jsonArray = new JsonArray(json);
		return this.paraLista(jsonArray);
	}

}
