package br.com.esilvasoft.geradorestrutural.console.enumerador;

import java.util.Optional;

public enum ParametroConsole {

	/**
	 * 
	 */
	NOMES(new String[] { "--nomes", "-N" }, "Lista de nomes \"['nome1','nome2']\""),
	/**
	 * 
	 */
	LEIAUTE(new String[] { "--leiaute", "-L" }, "Nome do leiaute a ser utilizado"),
	/**
	 * 
	 */
	ATRIBUTOS(new String[] { "--atributos", "-A" }, "Lista de atributos \"[{chave:'',valor:''},{chave:'',valor:''}]\""),
	/**
	 * 
	 */
	ARQUIVO(new String[] { "--arquivo", "-F" }, "Caminho do aquivo jsom a ser utilizado."),
	/**
	 * 
	 */
	JSON(new String[] { "--json", "-J" }, "Jsom a ser utilizado."),
	/**
	 * 
	 */
	CONFIGURAR(new String[] { "--configurar", "-C" }, "criar arquivos e pastas iniciais"),
	/**
	 * 
	 */
	AJUDA(new String[] { "--help", "--ajuda", "-H" }, "Lista de comados"),
	/**
	 * 
	 */
	VERBOSO(new String[] { "--verboso", "-V" }, "verboso");

	private final String[] comandos;
	private final String descricao;

	private ParametroConsole(final String[] comandos, final String descricao) {
		this.comandos = comandos;
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public static Optional<ParametroConsole> verificarComando(final String comandoParametro) {
		for (ParametroConsole parametroConsole : ParametroConsole.values()) {
			for (String comando : parametroConsole.comandos) {
				if (comando.equals(comandoParametro)) {
					return Optional.of(parametroConsole);
				}
			}
		}
		return Optional.empty();
	}

	@Override
	public String toString() {
		return String.join(",\t", this.comandos) + " -\t" + descricao;
	}

}
