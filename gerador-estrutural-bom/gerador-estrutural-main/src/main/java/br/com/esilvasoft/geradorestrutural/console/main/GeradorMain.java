package br.com.esilvasoft.geradorestrutural.console.main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.tools.ant.types.Commandline;

import br.com.esilvasoft.geradorestrutural.comando.ComandoCriarEntidade;
import br.com.esilvasoft.geradorestrutural.comando.ComandoCriarEntidades;
import br.com.esilvasoft.geradorestrutural.console.enumerador.ParametroConsole;
import br.com.esilvasoft.geradorestrutural.json.ChaveValorJsonConversor;
import br.com.esilvasoft.geradorestrutural.json.ComandoCriarEntidadesJsonConversor;
import br.com.esilvasoft.geradorestrutural.json.ProjetoJsonConversor;
import br.com.esilvasoft.geradorestrutural.modelo.ChaveValor;
import br.com.esilvasoft.geradorestrutural.modelo.Contexto;
import br.com.esilvasoft.geradorestrutural.modelo.Projeto;
import br.com.esilvasoft.geradorestrutural.processador.ProcessadorServico;
import br.com.esilvasoft.geradorestrutural.retorno.RetornoExecucao;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import io.vertx.core.json.JsonArray;

@QuarkusMain
public class GeradorMain implements QuarkusApplication {

	private static final String DIRETORIO_CONFIGURACAO = "./.gerador-estrutural";
	private static final String ARQUIVO_CONFIGURACAO = "./configuracao.json";

	private static final String LEIAUTE_PADRAO = "Padrao";

	private Boolean verboso = Boolean.FALSE;
	private Long inicioProcesso = 0l;

	@Inject
	ProcessadorServico processadorServico;

	@Inject
	ChaveValorJsonConversor chaveValorJsonConversor;

	@Inject
	ComandoCriarEntidadesJsonConversor comandoCriarEntidadesJsonConversor;

	@Inject
	ProjetoJsonConversor projetoJsonConversor;

	private Contexto contexto;
	private Projeto projeto;

	private void ajuda() {
		for (final ParametroConsole parametroConsole : ParametroConsole.values()) {
			System.out.println(parametroConsole.toString());
		}
	}

	private void carregarContexto() {
		final File diretorio = new File(GeradorMain.DIRETORIO_CONFIGURACAO);
		this.contexto = new Contexto(diretorio);
	}

	private void carregarProjeto() throws IOException {
		final File diretorio = this.contexto.getDiretorio();
		final File arquivo = new File(diretorio, GeradorMain.ARQUIVO_CONFIGURACAO);
		final String json = Files.readString(arquivo.toPath());
		this.projeto = this.projetoJsonConversor.paraEntidade(json);
	}

	private void configurar() throws IOException {
		// TODO refazer o configurador

		final File diretorio = new File(GeradorMain.DIRETORIO_CONFIGURACAO);
		final File arquivo = new File(diretorio, GeradorMain.ARQUIVO_CONFIGURACAO);
		final File diretorioLeiautePadrao = new File(diretorio, GeradorMain.LEIAUTE_PADRAO);
		final Path diretorioPath = diretorio.getAbsoluteFile().toPath();
		final Path arquivoPath = arquivo.getAbsoluteFile().toPath();
		final Path diretorioLeiautePadraoPath = diretorioLeiautePadrao.getAbsoluteFile().toPath();
		// final Projeto projeto = this.projetoFabrica.criarProjetoPadrao();
		// final String json = this.projetoFabrica.paraJson(projeto);

		if (Files.notExists(diretorioPath)) {
			Files.createDirectories(diretorioPath);
		}
		if (Files.notExists(diretorioLeiautePadraoPath)) {
			Files.createDirectories(diretorioLeiautePadraoPath);
		}
		if (Files.notExists(arquivoPath)) {
			Files.createFile(arquivoPath);
			// Files.writeString(arquivoPath, json, StandardOpenOption.WRITE);
		}

	}

	private int executarComando(final String... args) throws IOException {
		this.inicializar();
		this.inicioProcesso = System.currentTimeMillis();
		RetornoExecucao retornoExecucao = null;
		try {
			final List<String> parametros = new ArrayList<>(Arrays.asList(args));
			this.verificarVerboso(parametros);
			if (args.length > 0) {
				final Optional<ParametroConsole> comandoOptional = ParametroConsole
						.verificarComando(parametros.iterator().next());
				if (comandoOptional.isPresent()) {
					final ParametroConsole comando = comandoOptional.get();
					switch (comando) {
					case AJUDA:
						this.ajuda();
						return 0;

					case CONFIGURAR:
						this.configurar();
						return 0;
					default:
						break;
					}

					switch (comando) {
					case ARQUIVO:
						retornoExecucao = this.gerarPorArquivo(parametros);
						return 0;

					case JSON:
						retornoExecucao = this.gerarPorJson(parametros);
						return 0;

					case NOMES:
					case ATRIBUTOS:
					case LEIAUTE:
						retornoExecucao = this.gerarEmLote(parametros);
						return 0;
					default:
						retornoExecucao = this.gerar(parametros);
						return 0;
					}
				}

			}
		} finally {
			this.imprimir(retornoExecucao);
		}
		return 0;
	}

	private int executarConsole() throws IOException {
		@SuppressWarnings("resource")
		final Scanner scanner = new Scanner(System.in);
		String command;
		do {
			command = scanner.nextLine();
			final String[] args = Commandline.translateCommandline(command);
			this.executarComando(args);
		} while (!"exit".equals(command));

		return 0;

	}

	private RetornoExecucao gerar(final List<String> parametros) {
		final Iterator<String> parametrosIterable = parametros.iterator();
		String nomeLeiaute = GeradorMain.LEIAUTE_PADRAO;
		final String nome = parametrosIterable.next();
		List<ChaveValor> atributos = Collections.emptyList();

		while (parametrosIterable.hasNext()) {
			final String parametro = parametrosIterable.next();
			final Optional<ParametroConsole> comandoOptional = ParametroConsole.verificarComando(parametro);
			if (comandoOptional.isPresent()) {
				final ParametroConsole comando = comandoOptional.get();
				switch (comando) {
				case LEIAUTE:
					if (parametrosIterable.hasNext()) {
						nomeLeiaute = parametrosIterable.next();
					}
					break;
				case ATRIBUTOS:
					if (parametrosIterable.hasNext()) {
						atributos = this.chaveValorJsonConversor.paraLista(parametrosIterable.next());
					}
					break;
				default:
					// nada
					break;
				}

			}
		}

		final ComandoCriarEntidade comandoCriar = new ComandoCriarEntidade(this.projeto, nome, nomeLeiaute, atributos);
		return this.processadorServico.executar(this.contexto, comandoCriar);

	}

	private RetornoExecucao gerarEmLote(final List<String> parametros) {
		final Iterator<String> parametrosIterable = parametros.iterator();
		final List<String> nomes = new ArrayList<>();
		String nomeLeiaute = GeradorMain.LEIAUTE_PADRAO;
		List<ChaveValor> atributos = Collections.emptyList();

		while (parametrosIterable.hasNext()) {
			final String parametro = parametrosIterable.next();
			final Optional<ParametroConsole> comandoOptional = ParametroConsole.verificarComando(parametro);
			if (comandoOptional.isPresent()) {
				final ParametroConsole comando = comandoOptional.get();
				switch (comando) {
				case LEIAUTE:
					if (parametrosIterable.hasNext()) {
						nomeLeiaute = parametrosIterable.next();
					}
					break;
				case NOMES:
					if (parametrosIterable.hasNext()) {
						final JsonArray jsonArray = new JsonArray(parametrosIterable.next());
						for (final Object object : jsonArray) {
							nomes.add((String) object);
						}
					}
					break;
				case ATRIBUTOS:
					if (parametrosIterable.hasNext()) {
						atributos = this.chaveValorJsonConversor.paraLista(parametrosIterable.next());
					}
					break;
				default:
					// nada
					break;
				}

			}
		}
		final ComandoCriarEntidades comandoCriar = new ComandoCriarEntidades(this.projeto, nomeLeiaute, nomes,
				atributos);
		return this.processadorServico.executar(this.contexto, comandoCriar);
	}

	private RetornoExecucao gerarPorArquivo(final List<String> parametros) throws IOException {
		if (parametros.size() >= 2) {
			final Iterator<String> parametrosIterable = parametros.iterator();
			final String parametro = parametrosIterable.next();
			final Optional<ParametroConsole> comandoOptional = ParametroConsole.verificarComando(parametro);
			if (comandoOptional.isPresent()) {
				final ParametroConsole comando = comandoOptional.get();
				if (ParametroConsole.ARQUIVO.equals(comando)) {
					final Path arquivoPath = new File(parametrosIterable.next()).toPath();
					final String json = Files.readString(arquivoPath);
					final List<ComandoCriarEntidades> comandos = this.comandoCriarEntidadesJsonConversor
							.paraLista(this.projeto, json);
					return new RetornoExecucao(comandos.parallelStream()
							.map(c -> this.processadorServico.executar(this.contexto, c)).collect(Collectors.toList()));
				}
			}
		}
		return null;
	}

	private RetornoExecucao gerarPorJson(final List<String> parametros) {
		if (parametros.size() >= 2) {
			final Iterator<String> parametrosIterable = parametros.iterator();
			final String parametro = parametrosIterable.next();
			final Optional<ParametroConsole> comandoOptional = ParametroConsole.verificarComando(parametro);
			if (comandoOptional.isPresent()) {
				final ParametroConsole comando = comandoOptional.get();
				if (ParametroConsole.JSON.equals(comando)) {
					final String json = parametrosIterable.next();
					final List<ComandoCriarEntidades> comandos = this.comandoCriarEntidadesJsonConversor
							.paraLista(this.projeto, json);
					return new RetornoExecucao(comandos.parallelStream()
							.map(c -> this.processadorServico.executar(this.contexto, c)).collect(Collectors.toList()));
				}
			}
		}
		return null;
	}

	private void imprimir(final RetornoExecucao retornoExecucao) {
		Optional.ofNullable(retornoExecucao).ifPresent(execucao -> {
			if (!execucao.getErros().isEmpty()) {
				System.out.println("/*--------------------------------------------------*/");
				System.out.println("/*-- Erros -----------------------------------------*/");
				System.out.println("");

			}
			execucao.getErros().forEach(Exception::printStackTrace);
			if (this.verboso) {
				System.out.println("/*--------------------------------------------------*/");
				System.out.println("/*-- Arquivos Gerados                             --*/");
				System.out.println("/*--------------------------------------------------*/");
				execucao.getArquivosGerados().forEach(System.out::println);

			}
		});
		System.out.println("Tempo: " + (System.currentTimeMillis() - this.inicioProcesso));

	}

	private void inicializar() throws IOException {
		this.carregarContexto();
		this.carregarProjeto();
	}

	@Override
	public int run(final String... args) throws Exception {
		if (args.length > 0) {
			return this.executarComando(args);

		} else {
			return this.executarConsole();
		}
	}

	private void verificarVerboso(final List<String> parametros) {
		final Iterator<String> parametrosIterable = parametros.iterator();
		while (parametrosIterable.hasNext()) {
			ParametroConsole.verificarComando(parametrosIterable.next()).ifPresent(parmetro -> {
				if (ParametroConsole.VERBOSO.equals(parmetro)) {
					parametrosIterable.remove();
					this.verboso = Boolean.TRUE;
				}
			});
			;
		}
	}

}
