package br.com.esilvasoft.geradorestrutural.modelo;

import java.util.ArrayList;
import java.util.List;

public class Leiaute {

	private String nome;
	private List<String> extende = new ArrayList<>();
	private List<ChaveValor> atributos = new ArrayList<>();
	private List<ArquivoLeiaute> arquivos = new ArrayList<>();

	public List<ArquivoLeiaute> getArquivos() {
		return this.arquivos;
	}

	public List<ChaveValor> getAtributos() {
		return this.atributos;
	}

	public List<String> getExtende() {
		return this.extende;
	}

	public String getNome() {
		return this.nome;
	}

	public void setArquivos(final List<ArquivoLeiaute> arquivos) {
		this.arquivos = arquivos;
	}

	public void setAtributos(final List<ChaveValor> atributos) {
		this.atributos = atributos;
	}

	public void setExtende(final List<String> extende) {
		this.extende = extende;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

}
