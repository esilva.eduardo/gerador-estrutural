package br.com.esilvasoft.geradorestrutural.retorno;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class RetornoExecucao {

	private final List<Exception> erros;

	private final List<Path> arquivosGerados;

	public RetornoExecucao() {
		this.erros = new ArrayList<>();
		this.arquivosGerados = new ArrayList<>();
	}

	public RetornoExecucao(final Collection<RetornoExecucao> lista) {
		this.erros = lista.stream().flatMap(it -> it.getErros().stream()).collect(Collectors.toList());
		this.arquivosGerados = lista.parallelStream().flatMap(it -> it.getArquivosGerados().stream())
				.collect(Collectors.toList());
	}

	public void addArquivo(final Path path) {
		this.arquivosGerados.add(path);
	}

	public void addErro(final Exception e) {
		this.erros.add(e);
	}

	public List<Path> getArquivosGerados() {
		return this.arquivosGerados;
	}

	public List<Exception> getErros() {
		return this.erros;
	}
}