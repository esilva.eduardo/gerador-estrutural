package br.com.esilvasoft.geradorestrutural.modelo;

public class ArquivoLeiaute {
	private String diretorio;
	private String nome;
	private String tipo;
	private String leiaute;

	public String getDiretorio() {
		return this.diretorio;
	}

	public String getLeiaute() {
		return this.leiaute;
	}

	public String getNome() {
		return this.nome;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setDiretorio(final String diretorio) {
		this.diretorio = diretorio;
	}

	public void setLeiaute(final String leiaute) {
		this.leiaute = leiaute;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public void setTipo(final String tipo) {
		this.tipo = tipo;
	}

}
