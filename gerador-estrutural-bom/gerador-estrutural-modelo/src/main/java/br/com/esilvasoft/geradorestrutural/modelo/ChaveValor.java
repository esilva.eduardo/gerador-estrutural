package br.com.esilvasoft.geradorestrutural.modelo;

import java.util.Optional;

public class ChaveValor {
	private String chave;
	private Optional<String> valor;

	public String getChave() {
		return this.chave;
	}

	public Optional<String> getValor() {
		return this.valor;
	}

	public void setChave(final String chave) {
		this.chave = chave;
	}

	public void setValor(final Optional<String> valor) {
		this.valor = valor;
	}

	public void setValor(final String valor) {
		this.valor = Optional.ofNullable(valor);
	}

}
