package br.com.esilvasoft.geradorestrutural.modelo;

import java.io.File;

public class Contexto {

	private final File diretorio;

	public Contexto(final File diretorio) {
		super();
		this.diretorio = diretorio;
	}

	public File getDiretorio() {
		return this.diretorio;
	}

}
