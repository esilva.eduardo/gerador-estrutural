package br.com.esilvasoft.geradorestrutural.modelo;

import java.util.ArrayList;
import java.util.List;

public class Projeto {

	private List<ChaveValor> atributos = new ArrayList<>();

	private List<Leiaute> leiautes = new ArrayList<>();

	public List<ChaveValor> getAtributos() {
		return atributos;
	}

	public void setAtributos(List<ChaveValor> atributos) {
		this.atributos = atributos;
	}

	public List<Leiaute> getLeiautes() {
		return leiautes;
	}

	public void setLeiautes(List<Leiaute> leiautes) {
		this.leiautes = leiautes;
	}
}
