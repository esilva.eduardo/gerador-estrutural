package br.com.esilvasoft.geradorestrutural.enumerador;

public enum Chave {

	/**
	 * NomeNormal
	 */
	NOME("NOME"),
	/**
	 * nomeminusculo
	 */
	NOME_MINUSCULO("NOME_MINUSCULO"),

	/**
	 * NOMEMAIUSCULO
	 */
	NOME_MAIUSCULO("NOME_MAIUSCULO"),

	/**
	 * NomeInicioMaiusculo
	 */
	NOME_INICIO_MAIUSCULO("NOME_INICIO_MAIUSCULO"),

	/**
	 * nomeInicioMinusculo
	 */
	NOME_INICIO_MINUSCULO("NOME_INICIO_MINUSCULO"),

	/**
	 * nome-minusculo
	 */
	NOME_MINUSCULO_HIFEN("NOME_MINUSCULO_HIFEN"),

	/**
	 * NOME_MAIUSCULO
	 */
	NOME_MAIUSCULO_BARRA("NOME_MAIUSCULO_BARRA"),

	;

	private final String valor;

	private Chave(final String valor) {
		this.valor = valor;
	}

	public String getValor() {
		return this.valor;
	}

}
