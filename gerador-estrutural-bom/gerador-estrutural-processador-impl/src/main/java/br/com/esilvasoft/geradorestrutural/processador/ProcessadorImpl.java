package br.com.esilvasoft.geradorestrutural.processador;

import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;

import br.com.esilvasoft.geradorestrutural.comando.ComandoCriarEntidade;
import br.com.esilvasoft.geradorestrutural.comando.ComandoCriarEntidades;
import br.com.esilvasoft.geradorestrutural.enumerador.Chave;
import br.com.esilvasoft.geradorestrutural.modelo.Contexto;
import br.com.esilvasoft.geradorestrutural.retorno.RetornoExecucao;

@ApplicationScoped
public class ProcessadorImpl implements ProcessadorServico {

	private RetornoExecucao compilarLeiaute(final Contexto contexto, final ComandoCriarEntidades comando,
			final String nome) {
		final Compilador compilador = new Compilador(contexto.getDiretorio(), comando.getProjeto());
		compilador.carregarLeiaute(comando.getNomeLeiaute());
		this.gerarAtributos(compilador, nome);
		compilador.botarTodosAtributos(comando.getAtributos());
		compilador.gerarCompiladores();
		return compilador.compilarLeiaute(comando.getNomeLeiaute());
	}

	@Override
	public RetornoExecucao executar(final Contexto contexto, final ComandoCriarEntidade comando) {
		final Compilador compilador = new Compilador(contexto.getDiretorio(), comando.getProjeto());
		compilador.carregarLeiaute(comando.getNomeLeiaute());
		this.gerarAtributos(compilador, comando.getNome());
		compilador.botarTodosAtributos(comando.getAtributos());
		compilador.gerarCompiladores();
		return compilador.compilarLeiaute(comando.getNomeLeiaute());
	}

	@Override
	public RetornoExecucao executar(final Contexto contexto, final ComandoCriarEntidades comando) {
		return new RetornoExecucao(comando.getNomes().parallelStream()
				.map(nome -> this.compilarLeiaute(contexto, comando, nome)).collect(Collectors.toList()));
	}

	private void gerarAtributos(final Compilador compilador, final String nome) {
		final char[] letrasNome = nome.toCharArray();
		final StringBuilder builderHifen = new StringBuilder();
		final StringBuilder builderBarra = new StringBuilder();
		final StringBuilder builderMinusculoInicio = new StringBuilder();
		final StringBuilder builderMaiusculoInicio = new StringBuilder();

		for (int i = 0; i < letrasNome.length; i++) {
			final char letra = letrasNome[i];
			if (i == 0) {
				builderMinusculoInicio.append(Character.toLowerCase(letra));
				builderMaiusculoInicio.append(Character.toUpperCase(letra));
			} else {
				builderMinusculoInicio.append(letra);
				builderMaiusculoInicio.append(letra);
			}
			if (i > 0) {
				if (Character.isUpperCase(letra)) {
					builderHifen.append("-");
					builderBarra.append("_");
				}
			}
			builderHifen.append(letra);
			builderBarra.append(letra);
		}

		compilador.botarAtributo(Chave.NOME.getValor(), nome);
		compilador.botarAtributo(Chave.NOME_MINUSCULO.getValor(), nome.toLowerCase());
		compilador.botarAtributo(Chave.NOME_MAIUSCULO.getValor(), nome.toUpperCase());
		compilador.botarAtributo(Chave.NOME_MAIUSCULO_BARRA.getValor(), builderBarra.toString().toLowerCase());
		compilador.botarAtributo(Chave.NOME_MINUSCULO_HIFEN.getValor(), builderHifen.toString().toLowerCase());
		compilador.botarAtributo(Chave.NOME_INICIO_MINUSCULO.getValor(), builderMinusculoInicio.toString());
		compilador.botarAtributo(Chave.NOME_INICIO_MAIUSCULO.getValor(), builderMaiusculoInicio.toString());
	}

}
