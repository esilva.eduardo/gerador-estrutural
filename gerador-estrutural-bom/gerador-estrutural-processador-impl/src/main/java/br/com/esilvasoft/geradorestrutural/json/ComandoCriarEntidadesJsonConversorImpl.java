package br.com.esilvasoft.geradorestrutural.json;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import br.com.esilvasoft.geradorestrutural.comando.ComandoCriarEntidades;
import br.com.esilvasoft.geradorestrutural.modelo.ChaveValor;
import br.com.esilvasoft.geradorestrutural.modelo.Projeto;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

@ApplicationScoped
public class ComandoCriarEntidadesJsonConversorImpl implements ComandoCriarEntidadesJsonConversor {
	private static final String ATRIBUTOS = "atributos";
	private static final String NOMES = "nomes";
	private static final String NOME_LEIAUTE = "nomeLeiaute";

	@Inject
	ChaveValorJsonConversor chaveValorJsonConversor;

	@Override
	public ComandoCriarEntidades paraEntidade(final Projeto projeto, final JsonObject jsonObject) {
		final JsonArray atributosJson = jsonObject.getJsonArray(ComandoCriarEntidadesJsonConversorImpl.ATRIBUTOS);
		final JsonArray nomesJson = jsonObject.getJsonArray(ComandoCriarEntidadesJsonConversorImpl.NOMES);
		final String nomeLeiaute = jsonObject.getString(ComandoCriarEntidadesJsonConversorImpl.NOME_LEIAUTE);
		final List<String> nomes = new ArrayList<>();
		final List<ChaveValor> atributos = this.chaveValorJsonConversor.paraLista(atributosJson);
		for (final Object object : nomesJson) {
			nomes.add((String) object);
		}
		return new ComandoCriarEntidades(projeto, nomeLeiaute, nomes, atributos);
	}

}
