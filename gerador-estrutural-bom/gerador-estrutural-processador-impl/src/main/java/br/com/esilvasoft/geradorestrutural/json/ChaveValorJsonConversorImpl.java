package br.com.esilvasoft.geradorestrutural.json;

import javax.enterprise.context.ApplicationScoped;

import br.com.esilvasoft.geradorestrutural.modelo.ChaveValor;
import io.vertx.core.json.JsonObject;

@ApplicationScoped
public class ChaveValorJsonConversorImpl implements ChaveValorJsonConversor {

	private static final String CHAVE = "chave";
	private static final String VALOR = "valor";

	@Override
	public ChaveValor paraEntidade(final JsonObject jsonObject) {
		final ChaveValor chaveValor = new ChaveValor();
		chaveValor.setChave(jsonObject.getString(ChaveValorJsonConversorImpl.CHAVE));
		chaveValor.setValor(jsonObject.getString(ChaveValorJsonConversorImpl.VALOR));
		return chaveValor;
	}

}
