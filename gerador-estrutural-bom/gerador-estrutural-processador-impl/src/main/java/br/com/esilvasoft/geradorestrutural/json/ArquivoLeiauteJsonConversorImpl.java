package br.com.esilvasoft.geradorestrutural.json;

import javax.enterprise.context.ApplicationScoped;

import br.com.esilvasoft.geradorestrutural.modelo.ArquivoLeiaute;
import io.vertx.core.json.JsonObject;

@ApplicationScoped
public class ArquivoLeiauteJsonConversorImpl implements ArquivoLeiauteJsonConversor {

	private static final String DIRETORIO = "diretorio";
	private static final String NOME = "nome";
	private static final String TIPO = "tipo";
	private static final String LEIAUTE = "leiaute";

	@Override
	public ArquivoLeiaute paraEntidade(final JsonObject jsonObject) {
		final ArquivoLeiaute arquivoLeiaute = new ArquivoLeiaute();
		arquivoLeiaute.setDiretorio(jsonObject.getString(ArquivoLeiauteJsonConversorImpl.DIRETORIO));
		if (jsonObject.containsKey(ArquivoLeiauteJsonConversorImpl.NOME)) {
			arquivoLeiaute.setNome(jsonObject.getString(ArquivoLeiauteJsonConversorImpl.NOME));
		}
		if (jsonObject.containsKey(ArquivoLeiauteJsonConversorImpl.TIPO)) {
			arquivoLeiaute.setTipo(jsonObject.getString(ArquivoLeiauteJsonConversorImpl.TIPO));
		}
		if (jsonObject.containsKey(ArquivoLeiauteJsonConversorImpl.LEIAUTE)) {
			arquivoLeiaute.setLeiaute(jsonObject.getString(ArquivoLeiauteJsonConversorImpl.LEIAUTE));
		}
		return arquivoLeiaute;
	}

}
