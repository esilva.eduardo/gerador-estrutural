package br.com.esilvasoft.geradorestrutural.json;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import br.com.esilvasoft.geradorestrutural.modelo.Projeto;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

@ApplicationScoped
public class ProjetoJsonConversorImpl implements ProjetoJsonConversor {

	private static final String ATRIBUTOS = "atributos";
	private static final String LEIAUTES = "leiautes";

	@Inject
	ChaveValorJsonConversor chaveValorJsonConversor;

	@Inject
	LeiauteJsonConversor leiauteJsonConversor;

	@Override
	public Projeto paraEntidade(final JsonObject jsonObject) {
		final JsonArray atributosJson = jsonObject.getJsonArray(ProjetoJsonConversorImpl.ATRIBUTOS);
		final JsonArray leiautesJson = jsonObject.getJsonArray(ProjetoJsonConversorImpl.LEIAUTES);
		final Projeto projeto = new Projeto();
		projeto.setAtributos(this.chaveValorJsonConversor.paraLista(atributosJson));
		projeto.setLeiautes(this.leiauteJsonConversor.paraLista(leiautesJson));
		return projeto;
	}

}
