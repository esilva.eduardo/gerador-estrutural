package br.com.esilvasoft.geradorestrutural.processador;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import br.com.esilvasoft.geradorestrutural.modelo.ArquivoLeiaute;
import br.com.esilvasoft.geradorestrutural.modelo.ChaveValor;
import br.com.esilvasoft.geradorestrutural.modelo.Leiaute;
import br.com.esilvasoft.geradorestrutural.modelo.Projeto;
import br.com.esilvasoft.geradorestrutural.retorno.RetornoExecucao;

final class Compilador {

	private final Projeto projeto;
	private final File diretorio;
	private final Map<String, Leiaute> leiautes;
	private final List<ArquivoLeiaute> arquivos = new ArrayList<ArquivoLeiaute>();

	private final Map<String, String> atributos = new HashMap<>();
	private final Map<String, Pattern> compiladores = new HashMap<>();

	public Compilador(final File diretorio, final Projeto projeto) {
		this.projeto = projeto;
		this.diretorio = diretorio;
		this.leiautes = this.projeto.getLeiautes().stream().collect(Collectors.toMap(Leiaute::getNome, it -> it));
		this.botarTodosAtributos(this.projeto.getAtributos());
	}

	private void botarAtributo(final ChaveValor chaveValor) {
		chaveValor.getValor().ifPresent(it -> this.botarAtributo(chaveValor.getChave(), it));
	}

	public void botarAtributo(final String chave, final String valor) {
		this.atributos.put(chave, valor);
	}

	public void botarTodosAtributos(final List<ChaveValor> atributos) {
		atributos.forEach(this::botarAtributo);
	}

	private Optional<Leiaute> buscarLeiaute(final String nomeLeiaute) {
		return Optional.ofNullable(this.leiautes.get(nomeLeiaute));
	}

	public void carregarLeiaute(final String nomeLeiaute) {
		this.buscarLeiaute(nomeLeiaute).ifPresent(leiaute -> {
			leiaute.getExtende().forEach(this::carregarLeiaute);
			this.arquivos.addAll(leiaute.getArquivos());
			this.botarTodosAtributos(leiaute.getAtributos());
		});
	}

	private RetornoExecucao compilar(final Leiaute leiaute, final ArquivoLeiaute arquivoLeiaute) {
		if (Objects.isNull(arquivoLeiaute.getNome())
				|| (Objects.nonNull(arquivoLeiaute.getNome()) && arquivoLeiaute.getNome().isBlank())) {
			return this.compilarDiretorio(arquivoLeiaute);
		} else {
			return this.compilarArquivo(leiaute, arquivoLeiaute);
		}

	}

	public String compilar(final String entrada) {
		String texto = entrada;
		for (final Map.Entry<String, String> entry : this.atributos.entrySet()) {
			final String chave = entry.getKey();
			final String valor = entry.getValue();

			final StringBuffer buffer = new StringBuffer();
			final Pattern compilador = this.compiladores.get(chave);
			final Matcher ocorrencia = compilador.matcher(texto);
			while (ocorrencia.find()) {
				ocorrencia.appendReplacement(buffer, valor);
			}
			ocorrencia.appendTail(buffer);
			texto = buffer.toString();
		}
		return texto;

	}

	private RetornoExecucao compilarArquivo(final Leiaute leiaute, final ArquivoLeiaute arquivoLeiaute) {
		final RetornoExecucao retornoExecucao = new RetornoExecucao();
		final File diretorioBaseLeiaute = new File(this.diretorio, leiaute.getNome());
		final File diretorio = new File(this.compilar(arquivoLeiaute.getDiretorio()));
		final String nome = this.compilar(arquivoLeiaute.getNome());
		final String tipo = this.compilar(arquivoLeiaute.getTipo());
		final File leiauteArquivo = new File(diretorioBaseLeiaute, arquivoLeiaute.getLeiaute());
		final File arquivo = new File(diretorio, nome + "." + tipo);
		final Path leiautePath = leiauteArquivo.getAbsoluteFile().toPath();
		final Path diretorioPath = diretorio.getAbsoluteFile().toPath();
		final Path arquivoPath = arquivo.getAbsoluteFile().toPath();
		try {
			if (Files.notExists(diretorioPath)) {
				Files.createDirectories(diretorioPath);
				retornoExecucao.addArquivo(diretorioPath);
			}
			if (Files.notExists(arquivoPath)) {
				Files.createFile(arquivoPath);
				String conteudo = Files.readString(leiautePath);
				conteudo = this.compilar(conteudo);
				Files.writeString(arquivoPath, conteudo, StandardOpenOption.WRITE);
				retornoExecucao.addArquivo(arquivoPath);
			}
		} catch (final IOException e) {
			retornoExecucao.addErro(e);
		}
		return retornoExecucao;
	}

	private RetornoExecucao compilarDiretorio(final ArquivoLeiaute arquivoLeiaute) {
		final RetornoExecucao retornoExecucao = new RetornoExecucao();
		final File diretorio = new File(this.compilar(arquivoLeiaute.getDiretorio()));
		Path diretorioPath = diretorio.getAbsoluteFile().toPath();
		try {
			if (Files.notExists(diretorioPath)) {
				diretorioPath = Files.createDirectories(diretorioPath);
				retornoExecucao.addArquivo(diretorioPath);
			}
		} catch (final IOException e) {
			retornoExecucao.addErro(e);
		}
		return retornoExecucao;
	}

	public RetornoExecucao compilarLeiaute(final String nomeLeiaute) {
		final List<RetornoExecucao> retornos = new ArrayList<RetornoExecucao>();
		this.buscarLeiaute(nomeLeiaute).ifPresent(leiaute -> {
			leiaute.getExtende().forEach(this::compilarLeiaute);
			leiaute.getArquivos().parallelStream().map(arquivo -> this.compilar(leiaute, arquivo))
					.forEach(retornos::add);
		});
		return new RetornoExecucao(retornos);
	}

	private Pattern gerarCompilador(final String chave) {
		return Pattern.compile("#\\{(" + chave + ")\\}");
	}

	public void gerarCompiladores() {
		final Map<String, Pattern> compiladores = this.atributos.keySet().stream()
				.collect(Collectors.toMap(it -> it, this::gerarCompilador));
		this.compiladores.clear();
		this.compiladores.putAll(compiladores);
	}

}
