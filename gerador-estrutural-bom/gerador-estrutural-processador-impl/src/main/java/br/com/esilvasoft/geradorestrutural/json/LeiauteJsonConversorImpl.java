package br.com.esilvasoft.geradorestrutural.json;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import br.com.esilvasoft.geradorestrutural.modelo.Leiaute;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

@ApplicationScoped
public class LeiauteJsonConversorImpl implements LeiauteJsonConversor {

	private static final String NOME = "nome";
	private static final String EXTENDE = "extende";
	private static final String ATRIBUTOS = "atributos";
	private static final String ARQUIVOS = "arquivos";

	@Inject
	ChaveValorJsonConversor chaveValorFabrica;

	@Inject
	ArquivoLeiauteJsonConversor arquivoLeiauteJsonConversor;

	@Override
	public Leiaute paraEntidade(final JsonObject jsonObject) {
		final Leiaute leiaute = new Leiaute();
		leiaute.setNome(jsonObject.getString(LeiauteJsonConversorImpl.NOME));
		this.popularExtend(jsonObject, leiaute);
		final JsonArray atributosJsonArray = jsonObject.getJsonArray(LeiauteJsonConversorImpl.ATRIBUTOS);
		final JsonArray arquivosJsonArray = jsonObject.getJsonArray(LeiauteJsonConversorImpl.ARQUIVOS);
		leiaute.setAtributos(this.chaveValorFabrica.paraLista(atributosJsonArray));
		leiaute.setArquivos(this.arquivoLeiauteJsonConversor.paraLista(arquivosJsonArray));
		return leiaute;
	}

	private void popularExtend(final JsonObject jsonObject, final Leiaute leiaute) {
		final List<String> extende = new ArrayList<>();
		final JsonArray extendeJsonArray = jsonObject.getJsonArray(LeiauteJsonConversorImpl.EXTENDE);
		if (Objects.nonNull(extendeJsonArray)) {
			extendeJsonArray.forEach(it -> extende.add((String) it));
		}
		leiaute.setExtende(extende);
	}

}
